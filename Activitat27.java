import java.util.*;

public class Activitat27
{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED ="\u001B[31m";
    private final static int DESCRIPCION =0;
    private final static int TEMPS =2;
    private final static int MIN_TEMPS =5;
    private final static int MAX_TEMPS =1000;
    private final static int PRIORITAT =3;
    private final static int FINALIZADA = 4;
    static Scanner teclat;
    static final int MAX_TASQUES =20;
    private static List<String> currentList = new ArrayList<String>();
    public static void main(String[] args) {
        teclat = new Scanner(System.in);
        String [][] lista= new String [MAX_TASQUES][5] ;
        int contador=0;
        int numeroTasca=1;
        int numero=-1;
        while( numero!=6){
            switch (numero=menu()) {
                case 1:
                    contador= tasca(lista, contador);
                    break;
                case 2:
                    mostrarTasques(lista, numeroTasca, contador);
                    break;
                case 3:
                    mostrarPerTemps(numeroTasca,lista,contador);
                    break;
                case 4:
                    afegirTempsTasca(lista);
                    break;
                case 5:
                    marcarFinalitzada(lista, numeroTasca, contador);
                    break;
                case 6:
                    System.out.println(ANSI_PURPLE+"\nAdéu"+ANSI_RESET);
                    break;
                default:
                    System.out.println(ANSI_RED+"Error! Has de introduïr un valor entre 1 i 6"+ANSI_RESET);
            }}

    }

    public static int menu() {
        System.out.println();
        System.out.println("Benvingut a Batoi To-Do List");
        System.out.println("---------------------------");
        System.out.println("\nOPCIONS DISPONIBLES");
        System.out.println("1. Afegir una tasca");
        System.out.println("2. Mostrar tasques");
        System.out.println("3. Mostrar tasques ordenades per temps");
        System.out.println("4. Afegir temps");
        System.out.println("5. Marcar tasca com a finalitzada");
        System.out.println("6. Eixir del programa");
        System.out.print("Introdueix l'opció [1-6]: ");
        int choice = teclat.nextInt();
        return choice;
    }
    public static int tasca(String [][] lista, int contador) {
        descripcio(lista,contador);
        descripcion(lista, contador);
        tipo();
        tiempo(lista,contador);
        prioritat(lista,contador);
        finalizado(lista,contador);
        contador++;
        return contador;
    }

    public static void mostrarTasques (String[][] lista, int numeroTasca, int contador) {

        for (int i = 0; i < contador; i++) {
            numeroTasca= mostrarTodo(lista[i], numeroTasca);
            System.out.println();
        }

    }
    public static int mostrarTodo(String[] lista, int numeroTasca) {

        System.out.print(ANSI_PURPLE+"Tasca "+numeroTasca+" => \""+lista[DESCRIPCION]+"\" a realitzar en "+lista[TEMPS]
                +" minuts [Prioritat "+lista[PRIORITAT]+"] "+lista[FINALIZADA]+ANSI_RESET);
        numeroTasca++;
        return numeroTasca;
    }

    public static void descripcio(String [][]lista, int contador){
        System.out.print("Descripcio de la tasca: ");
        lista[contador][DESCRIPCION] = teclat.nextLine();
    }
    public static void descripcion(String [][]lista, int contador){
        lista[contador][DESCRIPCION] = teclat.nextLine();
    }

    public static void tipo (){
        String[] respuesta = {"Hardware", "Xarxa", "Programació", "Base de dades", "Altre"};
        System.out.print("\nTipus [Hardware, Xarxa, Programació, Base de dades, Altre]: ");
        String pantalla = teclat.next();
        boolean coincide=false;
        do {
            if (pantalla.equals(respuesta[0])
                    ||pantalla.equals(respuesta[1])
                    ||pantalla.equals(respuesta[2])
                    ||pantalla.equals(respuesta[3])
                    ||pantalla.equals(respuesta[4]))
            {
                coincide=true;
            }
            else {
                System.out.println(ANSI_RED+"Error! El valor introduït no és vàlid"+ANSI_RESET);
                System.out.print("\nTipus[Hardware, Xarxa, Programació, Base de dades, Altre]: ");
                pantalla = teclat.next();
            }
        }while (coincide==false);
    }
    public static void tiempo (String [][]lista, int contador){
        System.out.print("Temps estimat[5-1000]: ");
        int numero= teclat.nextInt();
        if (numero<5||numero>1000){
            do {
                System.out.println(ANSI_RED+"Error! Has de introduïr un valor entre 5 i 1000"+ANSI_RESET);
                System.out.print("\nTemps estimat[5-1000]: ");
                numero= teclat.nextInt();
            }while (numero<5||numero>1000);
        }
        String numCadena= String.valueOf(numero);
        lista [contador][TEMPS]= numCadena;
    }
    public static void prioritat(String [][]lista, int contador){
        String[] respuesta = {"Baixa", "Mitja", "Alta"};
        boolean coincide=false;
        System.out.print("Prioritat[Baixa, Mitja, Alta]: ");
        lista [contador][PRIORITAT]= teclat.next();
        do {
            if (lista[contador][PRIORITAT].equals(respuesta[0])
                    ||lista[contador][PRIORITAT].equals(respuesta[1])
                    ||lista[contador][PRIORITAT].equals(respuesta[2])
            )
            {
                coincide=true;
            }
            else {
                System.out.println(ANSI_RED+"Error! El valor introduït no és vàlid"+ANSI_RESET);
                System.out.print("\nPrioritat[Baixa, Mitja, Alta]: ");
                lista[contador][PRIORITAT] = teclat.next();
            }
        }while (coincide==false);
    }
    public static void finalizado (String [][]lista, int contador){
        System.out.print(ANSI_GREEN+"S'ha afegit la tasca satisfactòriament. La tasca ocupa la posició "+(contador+1)+ANSI_RESET);
        lista [contador][FINALIZADA]= "(Pendent)";
    }
    public static void mostrarPerTemps(int numeroTasca,String [][] lista,int contador) {
        for (int i = MIN_TEMPS; i <= MAX_TEMPS; i++){
            for (int j = 0; j <=numeroTasca; j++) {
                if (i== Integer.valueOf(lista[j][TEMPS])) {
                    mostrarTasques(lista,j,contador);
                }
            }
        }
    }


    public static void afegirTempsTasca(String [][]lista){
        System.out.println("Introdueix el número de la tasca: ");
        int fila= teclat.nextInt();
        if (lista[fila-1][FINALIZADA].equals("(Finalitzada)")){
            System.out.println(ANSI_RED+"No es permet afegir temps a una tasca finalitzada"+ANSI_RESET);
            return;
        }
        int numEntero = Integer.parseInt(lista[fila-1][TEMPS]);
        System.out.println("Quant de temps vols afegir?: ");
        int tiempoExtra= teclat.nextInt();
        numEntero=numEntero+tiempoExtra;
        String numCadena= String.valueOf(numEntero);
        lista[fila-1][TEMPS]=numCadena;
        System.out.println(ANSI_GREEN+"Temps afegit amb èxit"+ANSI_RESET);
    }
    public static void marcarFinalitzada(String [][] lista, int numeroTasca, int contador){
        System.out.println("Quina tasca vols finalitzar?");
        mostrarTasques(lista, numeroTasca, contador);
        System.out.println("Introdueix el número de la tasca a finalitzar: ");
        int tasca= teclat.nextInt();
        if (lista[tasca-1][FINALIZADA].equals("(Pendent)")){
            System.out.println(ANSI_GREEN+"Tasca 1 finalitzada satisfactòriament"+ANSI_RESET);
            lista[tasca-1][FINALIZADA]="(Finalitzada)";
        }else {
            System.out.println(ANSI_RED+"La tasca ja està finalitzada"+ANSI_RESET);
        }
    }
}
